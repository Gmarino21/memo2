class Chopper
  def chop(position, array)
    return array.index(position) if array.include?(position)

    -1
  end

  def sum(array)
    return 'vacio' if array.empty?

    sum = array.inject(:+)

    return 'demasiado grande' if sum >= 100

    a = sum.div(10)
    b = sum.modulo(10)

    return num_to_name(a) + ',' + num_to_name(b) unless a.zero?

    num_to_name(b)
  end

  private

  def num_to_name(number)
    numbers_to_name = {
      1 => 'uno',
      2 => 'dos',
      3 => 'tres',
      4 => 'cuatro',
      5 => 'cinco',
      6 => 'seis',
      7 => 'siete',
      8 => 'ocho',
      9 => 'nueve',
      0 => 'cero'
    }

    numbers_to_name[number]
  end
end
